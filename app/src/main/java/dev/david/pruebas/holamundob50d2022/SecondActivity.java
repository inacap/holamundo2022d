package dev.david.pruebas.holamundob50d2022;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import dev.david.pruebas.holamundob50d2022.vistas.fragmentos.InicioFragment;
import dev.david.pruebas.holamundob50d2022.vistas.fragmentos.SegundoFragment;

public class SecondActivity extends AppCompatActivity implements InicioFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        TextView tvSaludo = findViewById(R.id.tvSaludo);

        // Consultar los datos de la sesion
        SharedPreferences preferencias = getSharedPreferences("sesion.data", Context.MODE_PRIVATE);
        String usuario = preferencias.getString("usuario", "");
        tvSaludo.setText("Hola " + usuario + "!");

        // Mostrar el Fragment
        // Instanciar el fragment a mostrar
        InicioFragment fragment = new InicioFragment();

        // Iniciar el administrador de fragmentos
        FragmentManager manager = getSupportFragmentManager();

        // Asociar el fragmento a nuestro contenedor
        manager.beginTransaction().replace(R.id.flContenedor, fragment).commit();

        Button btSiguiente = findViewById(R.id.btNext);
        btSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment.cambiarTextoPrincpial("Cambiamos el texto");
                String nombre = fragment.obtenerNombre();
                Toast.makeText(getApplicationContext(), nombre, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onFragmentInteraction() {
        //Toast.makeText(getApplicationContext(), "EVENTO DESDE FRAGMENTO", Toast.LENGTH_SHORT).show();

        FragmentManager manager = getSupportFragmentManager();
        SegundoFragment s = new SegundoFragment();
        manager.beginTransaction().replace(R.id.flContenedor, s).commit();
    }
}











